const expect = require('chai').expect;
const mylib = require('../src/mylib');

describe("Our first unit test", ()=> {
    before ( () => {
        //initialization
        //create objects ..etc
        console.log("initializing completed.")

    });
    it("Can add 1 and 2 together", ()=> {
        //Tests
        expect(mylib.add(1,2)).equal(3, "1 + 2 is not 3, for some reason ")
    });

    it("Can divide right", ()=> {
        //Tests
        expect(mylib.divide(5,0)).equal(0, "Error you need to fix function, but don't know how with arrow function")
    });
    it("Can subtract 5-3", ()=> {
        //Tests
        expect(mylib.subtract(5,3)).equal(2, "5- 3 is not 2, for some reason ")
    });
    it("Can multiply 2 and 2 together", ()=> {
        //Tests
        expect(mylib.multiply(2,2)).equal(4, "2 * 2 is not 4, for some reason ")
    });
    after ( () => {
        //cleanup
        //for example shutdown the express server
        console.log("Testing complete") 

    })
});